#pragma once

namespace astm32 {

template <class LINE> class Pwm {
protected:
  static TIM_HandleTypeDef& _getHandle() {
    static TIM_HandleTypeDef timHandle;
    return timHandle;
  }

  static TIM_TypeDef *_getInstance();
  static uint8_t _getChannel();
  typedef Pwm<LINE> _self_t;

public:
  /*
   * Prescaler: Decide on the base tick and use for instance:
   * `(uint16_t)(HAL_RCC_GetPCLK1Freq() / 10000)` (for 10kHz). Period: Count of
   * ticks for one period.  For instance `10000` to get a period of 1 second.
   */
  static void init(uint16_t prescaler, uint16_t period,
                   uint8_t clockDivision = TIM_CLOCKDIVISION_DIV1,
                   uint8_t counterMode = TIM_COUNTERMODE_UP) {
    auto& timHandle = _getHandle();

    auto instance = LINE::getTim();

    if (instance == TIM4) {
      __HAL_RCC_TIM4_CLK_ENABLE();
    } else {
      assert(0 && "Add rule to enable tim clk.");
    }

    timHandle.Instance = instance;
    timHandle.Init.Prescaler = prescaler - 1;

    timHandle.Init.Period = period;
    timHandle.Init.ClockDivision = clockDivision;
    timHandle.Init.CounterMode = counterMode;

    HAL_TIM_Base_Init(&timHandle);
    HAL_TIM_PWM_Init(&timHandle);
  }

  static void setPulse(uint16_t pulse, uint8_t mode = TIM_OCMODE_PWM1,
                       uint8_t ocPolarity = TIM_OCPOLARITY_HIGH,
                       uint8_t ocFastMode = TIM_OCFAST_DISABLE) {
    auto& timHandle = _getHandle();
    auto channel = LINE::getTimChannel();

    if (pulse == 0) {
      HAL_TIM_PWM_Stop(&timHandle, channel);
      return;
    }

    TIM_OC_InitTypeDef timOCConfig;

    timOCConfig.Pulse = pulse - 1;
    timOCConfig.OCMode = mode;
    timOCConfig.OCPolarity = ocPolarity;
    timOCConfig.OCFastMode = ocFastMode;

    HAL_TIM_PWM_ConfigChannel(&timHandle, &timOCConfig, channel);
    HAL_TIM_PWM_Start(&timHandle, channel);
  }

  _self_t &operator=(uint16_t pulse) {
    _self_t::setPulse(pulse);
    return this;
  }
};
} // namespace astm32
