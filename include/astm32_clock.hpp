#pragma once

#include <stm32f1xx_hal.h>

namespace astm32 {

class SystemClock {
public:
  static void init() {
    RCC_ClkInitTypeDef rccClkInitStruct;
    RCC_OscInitTypeDef rccOscInitStruct;

    __HAL_RCC_PWR_CLK_ENABLE();

    uint8_t fLatency;

    rccOscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    rccOscInitStruct.HSEState = RCC_HSE_ON;
    rccOscInitStruct.LSEState = RCC_LSE_OFF;
    rccOscInitStruct.HSIState = RCC_HSI_OFF;
    rccOscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    rccOscInitStruct.PLL.PLLState = RCC_PLL_ON;
    rccOscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;

    // 8 MHz * 9 = 72 MHz SYSCLK
    rccOscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    rccOscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    fLatency = FLASH_LATENCY_2;

    HAL_RCC_OscConfig(&rccOscInitStruct);

    rccClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |
                                  RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    rccClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    rccClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    rccClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    rccClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    HAL_RCC_ClockConfig(&rccClkInitStruct, fLatency);
  }
};

} // namespace astm32
