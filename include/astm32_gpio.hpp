#pragma once

#include <stm32f1xx_hal.h>
#include <cassert>

namespace astm32 {

enum class Port { A, B, C, D };

template <enum Port PORT, uint16_t PIN> class Line {
public:
  static const auto port = PORT;
  static const auto pin = PIN;

  static TIM_TypeDef * getTim() {
    static_assert(PORT == Port::B && PIN == GPIO_PIN_7, "Add rule for getTim for this PIN");
    if (PORT == Port::B && PIN == GPIO_PIN_7) {
      return TIM4;
    }
    return NULL;
  }

  static uint8_t getTimChannel() {
    static_assert(PORT == Port::B && PIN == GPIO_PIN_7, "Add rule for getTimChannel for this PIN");
    if (PORT == Port::B && PIN == GPIO_PIN_7) {
      return TIM_CHANNEL_2;
    }
    return 0xFF;
  }

  /*
   * Mode: GPIO_MODE_*
   * Pull: GPIO_NOPULL || GPIO_PULL*
   * Speed: GPIO_SPEED_FREQ_*
   */
  static void init(uint8_t mode, uint8_t pull, uint8_t speed,
                       bool alternativeFunctionClock) {
    GPIO_InitTypeDef gpioConfig;

    gpioConfig.Mode = mode;
    gpioConfig.Pull = pull;
    gpioConfig.Speed = speed;

    GPIO_TypeDef *p;
    switch (PORT) {
    case Port::A:
      p = GPIOA;
      __HAL_RCC_GPIOA_CLK_ENABLE();
      break;
    case Port::B:
      p = GPIOB;
      __HAL_RCC_GPIOB_CLK_ENABLE();
      break;
    case Port::C:
      p = GPIOC;
      __HAL_RCC_GPIOC_CLK_ENABLE();
      break;
    case Port::D:
      p = GPIOD;
      __HAL_RCC_GPIOD_CLK_ENABLE();
      break;
    default:
      assert(0 && "Unknown port");
    }

    if (alternativeFunctionClock) {
      __HAL_RCC_AFIO_CLK_ENABLE();
    }

    gpioConfig.Pin = PIN;
    HAL_GPIO_Init(p, &gpioConfig);
  }
};
} // namespace astm32
