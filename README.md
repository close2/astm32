Either checkout the branch using:

`git submodule add -b stm32-blink https....`

or checkout the branch somewhere and use a symbolic link:

`git worktree add --track -b stm32-blink <path> <remote>/<branch>`

